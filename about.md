---
title: About
layout: page
---
![Profile Image]({% if site.external-image %}{{ site.picture }}{% else %}{{ site.url }}/{{ site.picture }}{% endif %})

<p>Simbiosi.org is a project founded by <a href="https://antenore.simbiosi.org">Antenore Gatta</a> with the aim of founding a community managed company, using and supporting only Free and Open Source technologies.</p>

<h2>Projects</h2>

<ul>
	<li><a href="https://gitlab.com/Remmina/Remmina">Remmina project</a></li>
	<li><a href="https://gitlab.com/antenore/simbiosi.org">Simbiosi.org repository</a></li>
</ul>
